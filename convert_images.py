#!/usr/bin/env python
try:
    from PIL import Image, ImageOps
except ImportError:
    import Image
    import ImageOps
import os
import glob
import sys
import json
import pprint
import math
import shutil
import fnmatch

pp = pprint.PrettyPrinter(indent=2)


def dprint(dict_object):
    print(json.dumps(dict_object, indent=4, sort_keys=True))


def convert(infile, outfile, extension):
    source_image = Image.open(infile)
    source_image.load()
    largeur, hauteur = source_image.size
    print "\t largeur=%s x hauteur=%s mode=%s" % (largeur, hauteur, source_image.mode)

    if source_image.mode in ('RGBA', 'LA') or (source_image.mode == 'P' and 'transparency' in source_image.info):
        dest_image = Image.new("RGB", source_image.size, (255, 255, 255))
        alpha = source_image.convert('RGBA').split()[-1]
        dest_image.paste(source_image, mask=alpha)  # 3 is the alpha channel
        # dest_image.save(outfile, 'JPEG', quality=90)
    elif source_image.mode in ('RGB', 'P'):
        dest_image = source_image
    elif source_image.mode in ('CMYK'):
        dest_image = source_image.convert('RGB')
    else:
        print "\t largeur=%s x hauteur=%s mode=%s" % (largeur, hauteur, source_image.mode)
        print source_image.info
        raise Exception("%s mode=%s: Image format not enabled" % (infile, source_image.mode))

    dest_image = dest_image.convert("RGB")

    if largeur > hauteur:
        # Mode paysage
        canvas_width = canvas_height = largeur
    else:
        # Mode portrait
        canvas_width = canvas_height = hauteur

    print "\t %sx%s => %sx%s" % (largeur, hauteur, canvas_width, canvas_height)
    # Center the image
    x1 = int(math.floor((canvas_width - largeur) / 2))
    y1 = int(math.floor((canvas_height - hauteur) / 2))

    new_image = Image.new("RGB", (canvas_width, canvas_height), (255, 255, 255))
    new_image.paste(dest_image, (x1, y1, x1 + largeur, y1 + hauteur))
    if canvas_width > 2048:
        print "\t resize image to 2048x2048"
        new_image = new_image.resize((2048, 2048), Image.ANTIALIAS)
    img_format = extension.lower().replace('.', '')
    if img_format == 'png':
        new_image.save(outfile, 'PNG')
    if img_format == "gif":
        new_image.save(outfile, 'GIF')
    if img_format == 'jpg':
        new_image.save(outfile, 'JPEG', quality=90)


# Make dirs
home = os.path.abspath(os.getcwd())
indir = os.path.join(home, "in")
outdir = os.path.join(home, "out")

if not os.path.exists(indir):
    os.makedirs(indir)
# Clean out dir
shutil.rmtree(outdir)
if not os.path.exists(outdir):
    os.makedirs(outdir)

# 1: get files and details
# filenames = glob.glob(os.path.join(indir, "**/*.[Gg][Ii][Ff]"))
# filenames.extend(glob.glob(os.path.join(indir, "**/*.[Pp][Nn][Gg]")))
# filenames.extend(glob.glob(os.path.join(indir, "**/*.[Jj][Pp][Gg]")))
matches = []
for root, dirnames, filenames in os.walk(indir):
    for filename in fnmatch.filter(filenames, '*.[Gg][Ii][Ff]'):
        matches.append(os.path.join(root, filename))
    for filename in fnmatch.filter(filenames, '*.[Pp][Nn][Gg]'):
        matches.append(os.path.join(root, filename))
    for filename in fnmatch.filter(filenames, '*.[Jj][Pp][Gg]'):
        matches.append(os.path.join(root, filename))
# pp.pprint(matches)
# sys.exit(0)
filenames = sorted(matches)
fichiers_a_traiter = {}
for fullpath_filename in filenames:
    detail_fichier = {}
    fichier = os.path.basename(fullpath_filename)
    fichier_dir = os.path.dirname(fullpath_filename)
    fichier_dir_rel = os.path.relpath(fichier_dir, indir)
    fichier_base, fichier_ext = os.path.splitext(fichier)
    detail_fichier['fichier'] = fichier  # Profile-Hung-Alu-Verticale-407263950-122x150.gif
    detail_fichier['dirname_abs'] = fichier_dir  # /home/tof/dev/convert_image/in
    detail_fichier['dirname_rel'] = fichier_dir_rel  # in
    detail_fichier['basename'] = fichier_base  # Profile-Hung-Alu-Verticale-407263950-122x150
    detail_fichier['extension'] = fichier_ext  # .gif
    detail_fichier['dimensions_ok'] = False
    detail_fichier['largeur'] = 0
    detail_fichier['hauteur'] = 0
    detail_fichier['reference_ok'] = False
    detail_fichier['reference_filename'] = ''
    detail_fichier['est_reference'] = False
    if '-' in fichier:
        parts = fichier_base.split('-')

        fichier_largeur = fichier_hauteur = 0
        if 'x' in parts[-1]:
            fichier_largeur, fichier_hauteur = map(int, parts[-1].split('x'))

        image = Image.open(fullpath_filename)
        image_largeur, image_hauteur = image.size

        if (image_largeur == fichier_largeur) and (image_hauteur == fichier_hauteur):
            detail_fichier['largeur'] = image_largeur
            detail_fichier['hauteur'] = image_hauteur
            detail_fichier['dimensions_ok'] = True
            fichier_ref = parts[:-1]
            fichier_ref = '-'.join(fichier_ref) + fichier_ext
            fullpath_fichier_ref = os.path.join(fichier_dir, fichier_ref)
            if fullpath_fichier_ref in filenames:
                detail_fichier['reference_ok'] = True
                detail_fichier['reference_filename'] = fichier_ref

    fichiers_a_traiter[detail_fichier['fichier']] = detail_fichier

# 2: find reference files
for key, value in fichiers_a_traiter.iteritems():
    if value['reference_filename'] in fichiers_a_traiter:
        fichiers_a_traiter[value['reference_filename']]['est_reference'] = True
    if value['hauteur'] == 0 and value['largeur'] == 0:
        fichiers_a_traiter[key]['est_reference'] = True

# dprint(fichiers_a_traiter)

# 3: remove transparent background on reference files
for key, value in fichiers_a_traiter.iteritems():
    if value['est_reference']:
        dirname_abs = value['dirname_abs']
        dirname_rel = value['dirname_rel']
        sourcefile = os.path.join(dirname_abs, key)
        destdir = os.path.join(outdir, dirname_rel)
        destfile = os.path.join(destdir, key)
        if not os.path.exists(destdir):
            os.makedirs(destdir)
        print "%s" % os.path.join(dirname_rel, key)
        convert(sourcefile, destfile, value['extension'])
